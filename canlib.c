/*
 *  @file VGFThermo/pid.c
 *  @date 14.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Процесс расчета ПИД регулятора и обслуживания таймеров
 */

#include <stdio.h>
#include <stdint.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <arm_math.h>
#include "hardware.h"
#include "eeprom.h"
#include "pb.h"
#include "pb_encode.h"
#include "canlib.h"
#include "canmsg.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "controller.pb.h"

static void CAN_Config();
portTASK_FUNCTION_PROTO(CAN_main, pvParameters);
static uint32_t crc32(const unsigned char *buf, size_t len);

static uint8_t DeviceID = 0;
xQueueHandle CANQueue = NULL;


uint32_t globalStatus = 0;
xSemaphoreHandle xCanMutex = NULL;
#define MUTEX_TIMEOUT	100
static void SaveSettings();


float ADCtoCelsius(int32_t adc_data) {
const double PolyADCtoT[] = {0, 24.6166190713, 0.0365860973, -5.1826709357E-3, 1.426219023E-4, -1.0679145965E-6};
double mv = (adc_data * 2500.0)/(8388608*32.0);
double res = 0.0;
	for (int i = 0; i < sizeof(PolyADCtoT)/sizeof(PolyADCtoT[0]); i++)
		res += PolyADCtoT[i] * pow(mv, i);

	return (float)res;
}


/**
 * @function SaveSettings
 * @brief Сохранение настроек в EEPROM
 * @param None
 * @retval None
 */
static void SaveSettings() {
uint8_t *buffer;
size_t bytes_written;

	buffer = (uint8_t *)pvPortMalloc(EEPROM_SIZE - EEPROM_OFFSET - sizeof(uint8_t) - sizeof(size_t));
	memset(buffer, 0, EEPROM_SIZE - EEPROM_OFFSET - sizeof(uint8_t) - sizeof(size_t));

pb_ostream_t stream = pb_ostream_from_buffer(buffer, EEPROM_SIZE - EEPROM_OFFSET - sizeof(uint8_t) - sizeof(size_t));
EepSettings settings;

	settings.HeatingTime_count = 8;
	/* TODO Если нужны настройки зон */
	for (int i = 0; i < settings.HeatingTime_count; i++) {
		settings.HeatingTime[i] = 0;
	}
	if (!pb_encode(&stream, EepSettings_fields, &settings))
		return;

	bytes_written = stream.bytes_written;
	EEP_WriteBuffer((uint8_t *)&bytes_written, EEPROM_OFFSET + sizeof(uint8_t), sizeof(bytes_written));
	EEP_WaitEepromStandbyState();

	EEP_WriteBuffer(buffer, EEPROM_OFFSET + sizeof(uint8_t) + sizeof(size_t), bytes_written);
	EEP_WaitEepromStandbyState();
	vPortFree(buffer);
}

/**
 * @function CAN_Config
 * @breif Инициализация и настройка периферии CAN.
 * @param None
 * @retval None
 */
static void CAN_Config() {
GPIO_InitTypeDef GPIO_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	/* CAN1 config */
	GPIO_PinAFConfig(CAN_RX_GPIO_PORT, CAN_RX_SOURCE, CAN_RX_AF);
	GPIO_PinAFConfig(CAN_TX_GPIO_PORT, CAN_TX_SOURCE, CAN_TX_AF);

	GPIO_InitStructure.GPIO_Pin = CAN_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(CAN_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN;
	GPIO_Init(CAN_RX_GPIO_PORT, &GPIO_InitStructure);

	CAN_CLK_INIT(CAN_CLK, ENABLE);

	CAN_DeInit(CANLIB_CAN);

CAN_InitTypeDef CAN_InitStructure;
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;

	/* 1 MBps ?? */
	CAN_InitStructure.CAN_BS1 = CAN_BS1_14tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_6tq;
	//CAN_InitStructure.CAN_Prescaler=1;        // 2000 kbit/s
	//CAN_InitStructure.CAN_Prescaler=2;        // 1000 kbit/s
	CAN_InitStructure.CAN_Prescaler=4;        //  500 kbit/s
	//CAN_InitStructure.CAN_Prescaler=5;        //  400 kbit/s
	//CAN_InitStructure.CAN_Prescaler=8;        	//  250 kbit/s
	//CAN_InitStructure.CAN_Prescaler=10;       //  200 kbit/s
	//CAN_InitStructure.CAN_Prescaler=16;       //  125 kbit/s
	//CAN_InitStructure.CAN_Prescaler=20;       //  100 kbit/s
	//CAN_InitStructure.CAN_Prescaler=40;       //   50 kbit/s
	//CAN_InitStructure.CAN_Prescaler=80;       //   40 kbit/s
	//CAN_InitStructure.CAN_Prescaler=200;      //   10 kbit/s
	//CAN_InitStructure.CAN_Prescaler=1023;     //    ganz langsam
	CAN_Init(CANLIB_CAN, &CAN_InitStructure);

	/* TODO Configure filters */
CAN_FilterInitTypeDef CAN_FilterInitStructure;
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = GENID(DeviceID, CANMSG_ACK) << 5; // 0x91
	CAN_FilterInitStructure.CAN_FilterIdLow = GENID(DeviceID, CANMSG_NACK) << 5; // 0xA1
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = GENID(DeviceID, CANMSG_DTH) << 5; // 0xC1
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = GENID(DeviceID, CANMSG_HTD) << 5; // 0xB1
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	CAN_FilterInitStructure.CAN_FilterNumber = 1;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = GENID(DeviceID, CANMSG_DATA) << 5; // 0xE1
	CAN_FilterInitStructure.CAN_FilterIdLow = GENID(0, CANMSG_SYNC) << 5; // 0x80
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = GENID(0, CANMSG_EMERGENCY) << 5; // 0x70
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = GENID(0, CANMSG_HEARTBEAT) << 5; // 0xD0
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FilterFIFO0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Configure CAN RX0 interrupt. Priority 13 */
	CAN_ITConfig(CANLIB_CAN, CAN_IT_FMP0, ENABLE);
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0D;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


/**
 * @function CANlib_Init
 * @breif Инициализация и запуск процесса обработки сообщений CAN
 * @param id - ID принимаемых сообщений
 * @retval None
 */
void CANLib_Init(uint8_t id) {
	DeviceID = id;
	CAN_Config();
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);

	CANQueue = xQueueCreate(4, sizeof(CanRxMsg));
	vSemaphoreCreateBinary(xCanMutex);
	xSemaphoreGive(xCanMutex);

	xTaskCreate(CAN_main,  (signed portCHAR *)"CAN", configMINIMAL_STACK_SIZE * 3, NULL, 2, NULL);
}

/**
 * @function SendToHost
 * @breif Формирование и передача ProtocolBuffer
 * @param status - Статус
 * @retval 0 - Успешное завершение, (-1) - Ошибка таймаута, (-2) - Ошибка приема (получен NACK), (-3) - Memory error
 */
static int32_t SendToHost(CANStatus *status) {
#define TX_BUFFER_SIZE			1*1024
uint8_t 	*buffer = NULL;
Protocol 	*protocol = NULL;
uint32_t 	crc;
CanTxMsg 	TxMessage;
CanRxMsg	RxMessage;
uint8_t 	num_messages; // Количество сообщений
size_t		bytes_to_send;

	protocol = (Protocol *)pvPortMalloc(sizeof(Protocol));
	if (protocol == NULL)
		return CAN_MEMORY_ERROR;

	buffer = (uint8_t *)pvPortMalloc(TX_BUFFER_SIZE);
	if (buffer == NULL) {
		vPortFree(protocol);
		return CAN_MEMORY_ERROR;
	}

pb_ostream_t stream = pb_ostream_from_buffer(buffer, TX_BUFFER_SIZE);

	if (xSemaphoreTake(xCanMutex, MUTEX_TIMEOUT) == pdTRUE) {
		/* TODO Заполнить структуру для отправки сообщения в PC */
		for (int i = 0; i < 8; i++) {
			protocol->DtH.TemperatureMCU = 24.0f;
		}
		xSemaphoreGive(xCanMutex);
	} else {
		vPortFree(protocol);
		vPortFree(buffer);
		return CAN_SEMPTH_ERROR;
	}

	memset(buffer, 0, TX_BUFFER_SIZE);
	if (!pb_encode(&stream, Protocol_fields, protocol)) {
		vPortFree(protocol);
		vPortFree(buffer);
		return CAN_PB_ERROR;
	}

	bytes_to_send = stream.bytes_written;

	crc = crc32(buffer, bytes_to_send);

	/* Сама передача */
	TxMessage.StdId = GENID(DeviceID, CANMSG_DTH);
	TxMessage.ExtId = 0;
	TxMessage.IDE = CAN_Id_Standard;
	TxMessage.RTR = CAN_RTR_Data;
	TxMessage.DLC = 6;

	TxMessage.Data[0] = bytes_to_send & 0xFF;
	TxMessage.Data[1] = (bytes_to_send & 0xFF00) >> 8;
	memcpy(&TxMessage.Data[2], &crc, 4);

	/* TODO Добавить таймаут */
	while (CAN_Transmit(CANLIB_CAN, &TxMessage) == CAN_TxStatus_NoMailBox);

	/* TODO Добавить проверку на входящее сообщение. Если пришло, то вывалиться с ошибкой */
	num_messages = bytes_to_send / 7; // Количество полных сообщений
	for (int i = 0; i < num_messages; i++) {
		TxMessage.StdId = GENID(DeviceID, CANMSG_DATA);
		TxMessage.ExtId = 0;
		TxMessage.IDE = CAN_Id_Standard;
		TxMessage.RTR = CAN_RTR_Data;
		TxMessage.DLC = 8;

		TxMessage.Data[0] = i;
		memcpy(&TxMessage.Data[1], &buffer[i * 7], 7);
		while (CAN_Transmit(CANLIB_CAN, &TxMessage) == CAN_TxStatus_NoMailBox);
	}

	/* Передать последнюю херню */
	if (bytes_to_send % 7) {
		TxMessage.StdId = GENID(DeviceID, CANMSG_DATA);
		TxMessage.ExtId = 0;
		TxMessage.IDE = CAN_Id_Standard;
		TxMessage.RTR = CAN_RTR_Data;
		TxMessage.DLC = 1 + bytes_to_send % 7;

		TxMessage.Data[0] = bytes_to_send / 7;
		memcpy(&TxMessage.Data[1], &buffer[bytes_to_send - bytes_to_send % 7], bytes_to_send % 7);
		while (CAN_Transmit(CANLIB_CAN, &TxMessage) == CAN_TxStatus_NoMailBox);
	}

	/* Освобождаем память */
	vPortFree(protocol);
	vPortFree(buffer);

	/* Ждем ACK или NACK */
	if (xQueueReceive(CANQueue, &RxMessage, (portTickType) 100) == pdTRUE) {
		if (GETMSG(RxMessage.StdId) == CANMSG_ACK)
			return CAN_OK;
		else
			return CAN_ACK_ERROR;

	} else {
		return CAN_TIMEOUT_ERROR;
	}

	return CAN_OK;
}


/**
 * @function ReceiveFromHost
 * @breif Формирование и передача ProtocolBuffer
 * @param RxMessage - Первое сообщение приема (поля размера, CRC)
 * @retval 0 - Успешное завершение, (-1) - Ошибка таймаута, (-2) - Ошибка приема (получен NACK), (-3) - Memory error
 */
static int32_t ReceiveFromHost(CanRxMsg *RxMessage, CANStatus *status) {
static uint8_t 	*buffer = NULL;
Protocol 		*protocol = NULL;
static size_t 	bytes_to_receive = 0;
static uint8_t	num_msg = 0;
static uint8_t 	received_msg = 0;
static uint32_t crc;
CanTxMsg		lm;

	if (GETMSG(RxMessage->StdId) == CANMSG_HTD) {
		/* Конфигурим первое вхождение в прием */
		bytes_to_receive = RxMessage->Data[0];
		bytes_to_receive |= (RxMessage->Data[1] << 8);

		num_msg = bytes_to_receive / 7;
		if ((bytes_to_receive % 7) > 0)
			num_msg++;
		received_msg = num_msg;

		buffer = (uint8_t *)pvPortMalloc(bytes_to_receive);
		if (buffer == NULL)
			return CAN_MEMORY_ERROR;

		memcpy(&crc, &RxMessage->Data[1], 4);
		return CAN_OK;
	}


	/* Пришло сообщение с данными, проверяем на валидность и копируем в буффер */
	if (GETMSG(RxMessage->StdId) == CANMSG_DATA) {
		/* Приемный буффер не был инициализирован */
		if (buffer == NULL)
			return CAN_MEMORY_ERROR;

		int index = RxMessage->Data[0];
		/* Индекс сообщения превышен */
		if (index >= num_msg) {
			vPortFree(buffer);
			buffer = NULL;
			return CAN_ACK_ERROR;
		}

		memcpy(&buffer[index * 7], &RxMessage->Data[1], RxMessage->DLC - 1);
		received_msg--;
	}

	/* Приняли все сообщения!! */
	if (received_msg == 0) {
		/* Malloc for protocol */
		protocol = (Protocol *)pvPortMalloc(sizeof(Protocol));
		if (protocol == NULL) {
			vPortFree(buffer);
			buffer = NULL;
			return CAN_MEMORY_ERROR;
		}

pb_istream_t stream = pb_istream_from_buffer(buffer, bytes_to_receive);
		if (!pb_decode(&stream, Protocol_fields, protocol)) {
			vPortFree(buffer);
			vPortFree(protocol);
			return CAN_PB_ERROR;
		}

		/* Захватываем семафор и дрочим переменные */
		if (xSemaphoreTake(xCanMutex, (portTickType)100) == pdTRUE) {

			/* TODO Тут обновляем настройки */
			xSemaphoreGive(xCanMutex);
			LED_RED(LED2);

			/* Send ACK */
			lm.StdId = GENID(DeviceID, CANMSG_ACK);
			lm.ExtId = 0;
			lm.IDE = CAN_Id_Standard;
			lm.RTR = CAN_RTR_Data;
			lm.DLC = 0;
			while (CAN_Transmit(CANLIB_CAN, &lm) == CAN_TxStatus_NoMailBox);

			vPortFree(buffer);
			vPortFree(protocol);
			buffer = NULL;
		} else {
			vPortFree(buffer);
			vPortFree(protocol);
			buffer = NULL;
			return CAN_SEMPTH_ERROR;
		}
	}
	return CAN_OK;
}


/**
 * @function CANlib_main
 * @breif Процесс обработки.
 */
portTASK_FUNCTION(CAN_main, pvParameters) {
CanRxMsg RxMessage;
CANStatus status;
CanTxMsg		lm;
	lm.StdId = GENID(DeviceID, CANMSG_HEARTBEAT);
	lm.RTR = CAN_RTR_Data;
	lm.DLC = 0;
	lm.IDE = CAN_Id_Standard;
	while (CAN_Transmit(CANLIB_CAN, &lm) == CAN_TxStatus_NoMailBox);

	for (;;) {
		if (xQueueReceive(CANQueue, &RxMessage, portMAX_DELAY) == pdTRUE) {
			/* Есть сообщение */
			switch (GETMSG(RxMessage.StdId)) {
			case CANMSG_SYNC:
				if (RxMessage.DLC == 0) {
					SendToHost(&status);
					LED_GREEN(LED2);
				}
				break;

			case CANMSG_HTD:
			case CANMSG_DATA:
				ReceiveFromHost(&RxMessage, &status);
				break;

			default:
				break;
			}
		}
	}
}



/*
  Name  : CRC-32
  Poly  : 0x04C11DB7    x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11
                       + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
  Init  : 0xFFFFFFFF
  Revert: true
  XorOut: 0xFFFFFFFF
  Check : 0xCBF43926 ("123456789")
  MaxLen: 268 435 455 байт (2 147 483 647 бит) - обнаружение
   одинарных, двойных, пакетных и всех нечетных ошибок
*/
const uint32_t Crc32Table[256] = {
    0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
    0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
    0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
    0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
    0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
    0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
    0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
    0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
    0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
    0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
    0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
    0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
    0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
    0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
    0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
    0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
    0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
    0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
    0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
    0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
    0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
    0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
    0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
    0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
    0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
    0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
    0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
    0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
    0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
    0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
    0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
    0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
    0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
    0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
    0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
    0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
    0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
    0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
    0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
    0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
    0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
    0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
    0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
    0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
    0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
    0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
    0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
    0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
    0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
    0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
    0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
    0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
    0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
    0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
    0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
    0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
    0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
    0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
    0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
    0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
    0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
    0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
    0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
    0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
};

static uint32_t crc32(const unsigned char *buf, size_t len) {
	uint32_t crc = 0xFFFFFFFF;
	while(len--)
		crc = (crc >> 8) ^ Crc32Table[(crc ^ *buf++) & 0xFF];
	return crc ^ 0xFFFFFFFF;
}

/**
 * @function CAN1_RX0_IRQHandler
 * @breif Обработчик прерывания по получению сообщения CAN
 */
void CAN1_RX0_IRQHandler(void) {
CanRxMsg RxMessage;
portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	/* TODO Почему без сбрасывания флага CAN_IT_FMP0? */
	CAN_Receive(CANLIB_CAN, CAN_FIFO0, &RxMessage);
	if (CANQueue != NULL)
		xQueueSendFromISR(CANQueue, &RxMessage, &xHigherPriorityTaskWoken);

	if (xHigherPriorityTaskWoken == pdTRUE)
		portYIELD();
}
