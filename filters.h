/*
 *  @file VGFThermo/filters.h
 *  @date 26.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Фильтр скользящего среднего окна.
 */

#ifndef FILTERS_H_
#define FILTERS_H_

/* WINDOW_SIZE равно степени 2, позволяет деление заменить на сдвиг вправо */
#define WINDOW_SIZE		4

typedef struct {
	uint8_t position;
	uint8_t index_max;
	uint8_t index_min;
	int32_t data[WINDOW_SIZE];
	int32_t average;
} Filter;


void FilterInit(Filter *f);
int32_t FilterCalc(Filter *f, int32_t x);


#endif /* FILTERS_H_ */
