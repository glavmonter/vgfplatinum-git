/*
 *  @file VGFThermo/lmp90100.c
 *  @date 24.12.2012
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief LMP90100 AFE routings.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <stm32f4xx.h>
#include "hardware.h"
#include "config.h"
#include "lmp90100.h"
#include "filters.h"
#include "canlib.h"

/* Delay for NSS down */
#define NSS_DELAY	1000

Filter Filters[8];

/**
 * @function LMP90100_SPIReadReg
 * Чтение регистра из LMP90100
 */
uint8_t LMP90100_SPIReadReg(SPI_TypeDef *ADC_SPI, uint8_t addr, uint8_t *pURA) {
uint8_t newURA, inst;

	newURA = (addr & LMP90100_URA_MASK) >> 4;

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_LOW();
	else
		ADC2_NSS_LOW();

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (*pURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, inst);

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, newURA);

		*pURA = newURA;
	}

	inst = LMP90100_READ_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, inst);

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, 0x00);

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_BSY) == SET);

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_HIGH();
	else
		ADC2_NSS_HIGH();

	return ADC_SPI->DR;
}


/**
 * @function LMP90100_SPIWriteReg
 * Запись регистра в LMP90100
 */
void LMP90100_SPIWrireReg(SPI_TypeDef *ADC_SPI, uint8_t addr, uint8_t value, uint8_t *pURA) {
uint8_t newURA, inst;

	newURA = (addr & LMP90100_URA_MASK) >> 4;
	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_LOW();
	else
		ADC2_NSS_LOW();

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (*pURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;

		/*!< Loop while DR register in not emplty */
		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, inst);

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, newURA);

		*pURA = newURA;
	}

	inst = LMP90100_WRITE_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, inst);

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, value);

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_HIGH();
	else
		ADC2_NSS_HIGH();
}


/**
 * @function TI_LMP90100_SPINormalStreamReadReg
 * Reads multiple configuration registers
 */
void LMP90100_SPINormalStreamReadReg(SPI_TypeDef *ADC_SPI, uint8_t addr,
									 uint8_t *buffer, uint8_t count, uint8_t *pURA) {
uint8_t newURA, inst;

	newURA = (addr & LMP90100_URA_MASK) >> 4;

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_LOW();
	else
		ADC2_NSS_LOW();

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (*pURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, inst);

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, newURA);

		*pURA = newURA;
	}

	inst = LMP90100_READ_BIT | LMP90100_SIZE_STREAM
							 | (addr & LMP90100_LRA_MASK);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, inst);

	for (int i = 0; i < count; i++) {
		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, 0x00);
		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_BSY) == SET);
		*(buffer + i) = ADC_SPI->DR;
	}

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_HIGH();
	else
		ADC2_NSS_HIGH();
}


/**
 * @function LMP90100_SPINormalStreamWriteReg
 * Writes values to multiple configuration registers.
 */
void LMP90100_SPINormalStreamWriteReg(SPI_TypeDef *ADC_SPI, uint8_t addr,
									  uint8_t *buffer, uint8_t count, uint8_t *pURA) {
uint8_t newURA, inst;

	newURA = (addr & LMP90100_URA_MASK) >> 4;

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_LOW();
	else
		ADC2_NSS_LOW();

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (*pURA != newURA) {
		inst = LMP90100_INSTRUCTION_BYTE1_WRITE;

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, inst);

		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, newURA);

		*pURA = newURA;
	}

	inst = LMP90100_WRITE_BIT | LMP90100_SIZE_STREAM
							  | (addr & LMP90100_LRA_MASK);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, inst);

	for (int i = 0; i < count; i++) {
		while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
		SPI_I2S_SendData(ADC_SPI, *(buffer + i));
	}

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_HIGH();
	else
		ADC2_NSS_HIGH();
}


void LMP90100_SPIDataOnlyReadADC(SPI_TypeDef *ADC_SPI, uint8_t *buffer, uint8_t count) {
	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_LOW();
	else
		ADC2_NSS_LOW();

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	while (!(ADC_SPI->SR & SPI_I2S_FLAG_TXE));

	for (int i = 0; i < count; i++) {
		ADC_SPI->DR = 0x00;
		while (!(ADC_SPI->SR & SPI_I2S_FLAG_TXE));
		while (!(ADC_SPI->SR & SPI_I2S_FLAG_RXNE));
		while (ADC_SPI->SR & SPI_I2S_FLAG_BSY);

		*(buffer + i) = ADC_SPI->DR;
	}


	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_HIGH();
	else
		ADC2_NSS_HIGH();
}

/**
 * @function LMP90100_SPIEnableDataFirstMode
 * Enables the data first mode of LMP90100.
 */
void LMP90100_SPIEnableDataFirstMode(SPI_TypeDef *ADC_SPI, uint8_t addr, uint8_t count, uint8_t *pURA) {
	LMP90100_SPIWrireReg(ADC_SPI, LMP90100_DATA_ONLY_1_REG, addr, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, LMP90100_DATA_ONLY_2_REG, count - 1, pURA);

	/* NSS enable */
	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_LOW();
	else
		ADC2_NSS_LOW();

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, LMP90100_DATA_FIRST_MODE_INSTRUCTION_ENABLE);

	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(ADC_SPI, LMP90100_DATA_FIRST_MODE_INSTRUCTION_READ_MODE_STATUS);
	while (SPI_I2S_GetFlagStatus(ADC_SPI, SPI_I2S_FLAG_TXE) == RESET);

	while (ADC_SPI->DR & LMP90100_DATA_FIRST_MODE_STATUS_FLAG) {
		SPI_I2S_SendData(ADC_SPI, LMP90100_DATA_FIRST_MODE_INSTRUCTION_READ_MODE_STATUS);
		while (!(ADC_SPI->SR & SPI_I2S_FLAG_TXE));
		while (!(ADC_SPI->SR & SPI_I2S_FLAG_RXNE));
		while (ADC_SPI->SR & SPI_I2S_FLAG_BSY);
	}

	for (int i = 0; i < NSS_DELAY; i++)
		__ASM volatile ("nop");

	/* NSS disable */
	if (ADC_SPI == ADC1_SPI)
		ADC1_NSS_HIGH();
	else
		ADC2_NSS_HIGH();
}



/**
 * @function ADCsSetupSPI
 * Инициализация SPI2 и SPI3
 */
static void ADCsSetupSPI() {
GPIO_InitTypeDef 	GPIO_InitStructure;
SPI_InitTypeDef		SPI_InitStructure;
EXTI_InitTypeDef	EXTI_InitStructure;
NVIC_InitTypeDef	NVIC_InitStructure;

	/* Enable SPI Clock */
	ADC1_SPI_CLK_INIT(ADC1_SPI_CLK, ENABLE);
	ADC2_SPI_CLK_INIT(ADC2_SPI_CLK, ENABLE);

	/* Connect SPI pins to AF5 (SPI2) */
	GPIO_PinAFConfig(ADC1_SPI_SCK_GPIO_PORT, ADC1_SPI_SCK_SOURCE, ADC1_SPI_SCK_AF);
	GPIO_PinAFConfig(ADC1_SPI_MISO_GPIO_PORT, ADC1_SPI_MISO_SOURCE, ADC1_SPI_MISO_AF);
	GPIO_PinAFConfig(ADC1_SPI_MOSI_GPIO_PORT, ADC1_SPI_MOSI_SOURCE, ADC1_SPI_MOSI_AF);

	/* Connect SPI pins to AF6 (SPI3) */
	GPIO_PinAFConfig(ADC2_SPI_SCK_GPIO_PORT, ADC2_SPI_SCK_SOURCE, ADC2_SPI_SCK_AF);
	GPIO_PinAFConfig(ADC2_SPI_MISO_GPIO_PORT, ADC2_SPI_MISO_SOURCE, ADC2_SPI_MISO_AF);
	GPIO_PinAFConfig(ADC2_SPI_MOSI_GPIO_PORT, ADC2_SPI_MOSI_SOURCE, ADC2_SPI_MOSI_AF);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	/* SPI2 SCK */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_SCK_PIN;
	GPIO_Init(ADC1_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	/* SPI2 MISO */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_MISO_PIN;
	GPIO_Init(ADC1_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	/* SPI2 MOSI */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_MOSI_PIN;
	GPIO_Init(ADC1_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

	/* SPI3 SCK */
	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_SCK_PIN;
	GPIO_Init(ADC2_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	/* SPI3 MISO */
	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_MISO_PIN;
	GPIO_Init(ADC2_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	/* SPI3 MOSI */
	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_MOSI_PIN;
	GPIO_Init(ADC2_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);


	/* SPI2 NSS */
	GPIO_InitStructure.GPIO_Pin = ADC1_SPI_NSS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(ADC1_SPI_NSS_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ADC2_SPI_NSS_PIN;
	GPIO_Init(ADC2_SPI_NSS_GPIO_PORT, &GPIO_InitStructure);

	/* DRDY pins */
	GPIO_InitStructure.GPIO_Pin = ADC1_DRDY_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(ADC1_DRDY_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = ADC2_DRDY_PIN;
	GPIO_Init(ADC2_DRDY_GPIO_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Connect EXTI Line1 to PD1 */
	SYSCFG_EXTILineConfig(ADC1_DRDY_EXTI_SOURCE, ADC1_DRDY_EXTI_PIN);
	EXTI_InitStructure.EXTI_Line = EXTI_Line1;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set EXTI Line1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0E;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	/* Connect EXTI Line 0 to PD0 */
	SYSCFG_EXTILineConfig(ADC2_DRDY_EXTI_SOURCE, ADC2_DRDY_EXTI_PIN);
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set EXTI Line0 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0E;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	ADC1_NSS_HIGH();
	ADC2_NSS_HIGH();

	SPI_I2S_DeInit(ADC1_SPI);
	SPI_I2S_DeInit(ADC2_SPI);
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;

	SPI_Init(ADC1_SPI, &SPI_InitStructure);
	SPI_Cmd(ADC1_SPI, ENABLE);

	SPI_Init(ADC2_SPI, &SPI_InitStructure);
	SPI_Cmd(ADC2_SPI, ENABLE);
}


void LMP90100_WriteRegSettings(SPI_TypeDef *ADC_SPI, uint8_t *pURA, uint8_t gain[4]) {

	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_RESETCN_REG,
									LMP90100_RESETCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_SPI_HANDSHAKECN_REG,
									LMP90100_SPI_HANDSHAKECN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_SPI_STREAMCN_REG,
									LMP90100_SPI_STREAMCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_PWRCN_REG,
									LMP90100_PWRCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_ADC_RESTART_REG,
									LMP90100_ADC_RESTART_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_GPIO_DIRCN_REG,
									LMP90100_GPIO_DIRCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_GPIO_DAT_REG,
									LMP90100_GPIO_DAT_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_BGCALCN_REG,
									LMP90100_BGCALCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_SPI_DRDYBCN_REG,
									LMP90100_SPI_DRDYBCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_ADC_AUXCN_REG,
									LMP90100_ADC_AUXCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_SPI_CRC_CN_REG,
									LMP90100_SPI_CRC_CN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_SENDIAG_THLDH_REG,
									LMP90100_SENDIAG_THLDH_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_SENDIAG_THLDL_REG,
									LMP90100_SENDIAG_THLDL_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_SCALCN_REG,
									LMP90100_SCALCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_ADC_DONE_REG,
									LMP90100_ADC_DONE_REG_VALUE, pURA);

portTickType nextTick;
	nextTick = xTaskGetTickCount() + 1000; // Не более 1000 тиков читаем статус DAC
	while ((LMP90100_SPIReadReg(ADC_SPI, (LMP90100_CH_STS_REG & LMP90100_CH_SCAN_NRDY), pURA)) && (xTaskGetTickCount() < nextTick));

	LMP90100_SPIWrireReg(ADC_SPI, 	LMP90100_CH_SCAN_REG,
									LMP90100_CH_SCAN_REG_VALUE, pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH0_INPUTCN_REG,
									LMP90100_CH0_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH0_CONFIG_REG,
									gain[0], pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH1_INPUTCN_REG,
									LMP90100_CH1_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH1_CONFIG_REG,
									gain[1], pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH2_INPUTCN_REG,
									LMP90100_CH2_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH2_CONFIG_REG,
									gain[2], pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH3_INPUTCN_REG,
									LMP90100_CH3_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH3_CONFIG_REG,
									gain[3], pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH4_INPUTCN_REG,
									LMP90100_CH4_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH4_CONFIG_REG,
									LMP90100_CH4_CONFIG_REG_VALUE, pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH5_INPUTCN_REG,
									LMP90100_CH5_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH5_CONFIG_REG,
									LMP90100_CH5_CONFIG_REG_VALUE, pURA);

	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH6_INPUTCN_REG,
									LMP90100_CH6_INPUTCN_REG_VALUE, pURA);
	LMP90100_SPIWrireReg(ADC_SPI,	LMP90100_CH6_CONFIG_REG,
									LMP90100_CH6_CONFIG_REG_VALUE, pURA);
}

uint8_t LMP90100_crc8MakeBitwise2(uint8_t crc, uint8_t poly, uint8_t *pmsg, uint8_t msg_size) {
uint8_t msg;
	for (int i = 0 ; i < msg_size ; i++) {
		msg = (*pmsg++ << 0);
		for (int j = 0 ; j < 8 ; j++) {
			if((msg ^ crc) >> 7)
				crc = (crc << 1) ^ poly;
			else crc <<= 1;
		msg <<= 1;
		}
	}
	return(crc ^ CRC8_FINAL_XOR);
}

uint8_t LMP90100_SPICRCCheck (uint8_t *buffer, uint8_t count) {
uint8_t crc_data, crc_calc;

	crc_data = buffer[count];                                                    // extract CRC data
	crc_calc = LMP90100_crc8MakeBitwise2(CRC8_INIT_REM, CRC8_POLY,
                                           buffer, count);                     // calculate CRC for the adc output (size 3 bytes)
	if (crc_data == crc_calc)
		return CRC_PASS;
	else
		return CRC_FAIL;
}


#define CH_DATA_SIZE 	5	// 5 bytes: Status, ADC_DOUT2, ADC_DOUT1, ADC_DOUT0, CRC

portTASK_FUNCTION_PROTO(vADC1Data, pvParameters);
portTASK_FUNCTION_PROTO(vADC2Data, pvParameters);

/**
 * @function LMPInit
 * Инициализация порта SPI и начальная настройка AFE.
 * И некоторые простые проверки на чтение/запись регистров
 * @return None
 */
void LMP_Init(/* SettingsZones *settings */) {
	ADCsSetupSPI();

	for (int i = 0; i < sizeof(Filters)/sizeof(Filter); i++)
		FilterInit(&Filters[i]);

uint8_t *gain = (uint8_t *)pvPortMalloc(sizeof(uint8_t) * 4);
	for (int i = 0; i < 4; i++) {
		switch (128) {
		case 1:
			gain[i] = LMP90100_SPEED | (0x0 << 1);
			break;
		case 2:
			gain[i] = LMP90100_SPEED | (0x1 << 1);
			break;
		case 4:
			gain[i] = LMP90100_SPEED | (0x2 << 1);
			break;
		case 8:
			gain[i] = LMP90100_SPEED | (0x3 << 1);
			break;
		case 16:
			gain[i] = LMP90100_SPEED | (0x4 << 1);
			break;
		case 32:
			gain[i] = LMP90100_SPEED | (0x5 << 1);
			break;
		case 64:
			gain[i] = LMP90100_SPEED | (0x6 << 1);
			break;
		case 128:
			gain[i] = LMP90100_SPEED | (0x7 << 1);
			break;
		default:
			gain[i] = LMP90100_SPEED | (0x0 << 1); // Gain = 1
			break;
		}
	}
	xTaskCreate(vADC1Data, (signed portCHAR *)"ADC1", configMINIMAL_STACK_SIZE * 3, (void *)gain, 1, NULL);

	gain = (uint8_t *)pvPortMalloc(sizeof(uint8_t) * 4);
	for (int i = 0; i < 4; i++) {
		switch (128) {
		case 1:
			gain[i] = LMP90100_SPEED | (0x0 << 1);
			break;
		case 2:
			gain[i] = LMP90100_SPEED | (0x1 << 1);
			break;
		case 4:
			gain[i] = LMP90100_SPEED | (0x2 << 1);
			break;
		case 8:
			gain[i] = LMP90100_SPEED | (0x3 << 1);
			break;
		case 16:
			gain[i] = LMP90100_SPEED | (0x4 << 1);
			break;
		case 32:
			gain[i] = LMP90100_SPEED | (0x5 << 1);
			break;
		case 64:
			gain[i] = LMP90100_SPEED | (0x6 << 1);
			break;
		case 128:
			gain[i] = LMP90100_SPEED | (0x7 << 1);
			break;
		default:
			gain[i] = LMP90100_SPEED | (0x0 << 1);
			break;
		}
	}
	xTaskCreate(vADC2Data, (signed portCHAR *)"ADC2", configMINIMAL_STACK_SIZE * 3, (void *) gain, 1, NULL);
}


xSemaphoreHandle ADC1Semph = NULL;
xSemaphoreHandle ADC2Semph = NULL;

/**
 * @function vADC1Data
 * Процесс обработки прерывания для ADC1
 */
portTASK_FUNCTION(vADC1Data, pvParameters) {
uint8_t count, addr, crc_test;
uint8_t read_buf[CH_DATA_SIZE];
int32_t adc_data;
uint8_t prevURA = LMP90100_URA_END;
uint32_t err = 0;
uint8_t ch_num;
uint8_t *gain = (uint8_t *)pvParameters;
uint8_t sendiag = 0;

	vSemaphoreCreateBinary(ADC1Semph);
	vTaskDelay(37);

	LMP90100_WriteRegSettings(ADC1_SPI, &prevURA, gain);

	addr = LMP90100_SENDIAG_FLAGS_REG;
	count = CH_DATA_SIZE;

	LMP90100_SPIEnableDataFirstMode(ADC1_SPI, addr, count, &prevURA);

	for (;;) {
		/* Wait for DRDY is low */
		if (xSemaphoreTake(ADC1Semph, 0xFFFF)) {

			LMP90100_SPIDataOnlyReadADC(ADC1_SPI, read_buf, count);
			adc_data = ((uint32_t)read_buf[1] << 16)
							| ((uint32_t)read_buf[2] << 8) | (uint32_t)read_buf[3];

			crc_test = LMP90100_SPICRCCheck(read_buf, CH_DATA_SIZE - 1);
			if (crc_test == CRC_PASS) {
				GPIO_ToggleBits(LED3_PORT, LED3_PIN);

				ch_num = read_buf[0] & LMP90100_CH_NUM_MASK;
				sendiag = read_buf[0] & 0xF8;
				if (adc_data & 0x800000) {
					/* Отрицательное число */
					adc_data = -(((~adc_data) & 0xffffff) + 1);
				}

				/* Проверка на правильный номер канала, фильтрация и отображение данных на консоль */
				switch (ch_num) {
				case 0:
					adc_data = FilterCalc(&Filters[0], adc_data);
					break;

				case 1:
					adc_data = FilterCalc(&Filters[1], adc_data);
					break;

				case 2:
					adc_data = FilterCalc(&Filters[2], adc_data);
					break;

				case 3:
					adc_data = FilterCalc(&Filters[3], adc_data);
					break;

				default:
					err++;
					break;
				}
			}
		}
	}
}


/**
  * @brief  This function handles External line 1 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI1_IRQHandler(void) {
static signed portBASE_TYPE xHigherPriorityTaskWoken;

	if(EXTI_GetITStatus(EXTI_Line1) != RESET) {
		/* Clear the EXTI line 1 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line1);

		if (ADC1Semph != NULL)
			xSemaphoreGiveFromISR(ADC1Semph, &xHigherPriorityTaskWoken);
	}

	if (xHigherPriorityTaskWoken == pdTRUE)
		portYIELD();
}

/**
 * @function vADC2Data
 * Процесс обработки прерывания для ADC2
 */
portTASK_FUNCTION(vADC2Data, pvParameters) {
uint8_t count, addr, crc_test;
uint8_t read_buf[CH_DATA_SIZE];
int32_t adc_data;
uint8_t prevURA = LMP90100_URA_END;
uint32_t err = 0;
uint8_t ch_num;
uint8_t *gain = (uint8_t *)pvParameters;
uint8_t sendiag = 0;

	vSemaphoreCreateBinary(ADC2Semph);

	vTaskDelay(74);

	LMP90100_WriteRegSettings(ADC2_SPI, &prevURA, gain);

	addr = LMP90100_SENDIAG_FLAGS_REG;
	count = CH_DATA_SIZE;

	LMP90100_SPIEnableDataFirstMode(ADC2_SPI, addr, count, &prevURA);

	for (;;) {
		/* Wait for DRDY is low */
		if (xSemaphoreTake(ADC2Semph, 0xFFFF)) {

			LMP90100_SPIDataOnlyReadADC(ADC2_SPI, read_buf, count);
			adc_data = ((uint32_t)read_buf[1] << 16)
							| ((uint32_t)read_buf[2] << 8) | (uint32_t)read_buf[3];

			crc_test = LMP90100_SPICRCCheck(read_buf, CH_DATA_SIZE - 1);
			if (crc_test == CRC_PASS) {
				GPIO_ToggleBits(LED4_PORT, LED4_PIN);

				ch_num = read_buf[0] & LMP90100_CH_NUM_MASK;
				sendiag = read_buf[0] & 0xF8;
				if (adc_data & 0x800000) {
					/* Отрицательное число */
					adc_data = -(((~adc_data) & 0xffffff) + 1);
				}

				switch (ch_num) {
				case 0:
					adc_data = FilterCalc(&Filters[4], adc_data);
					break;

				case 1:
					adc_data = FilterCalc(&Filters[5], adc_data);
					break;

				case 2:
					adc_data = FilterCalc(&Filters[6], adc_data);
					break;

				case 3:
					adc_data = FilterCalc(&Filters[7], adc_data);
					break;

				default:
					err++;
					break;
				}
			}
		}
	}
}

/**
  * @brief  This function handles External line 0 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI0_IRQHandler(void) {
static signed portBASE_TYPE xHigherPriorityTaskWoken;

	if(EXTI_GetITStatus(EXTI_Line0) != RESET) {
		if (ADC2Semph != NULL)
			xSemaphoreGiveFromISR(ADC2Semph, &xHigherPriorityTaskWoken);

		/* Clear the EXTI line 0 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line0);
	}

	if (xHigherPriorityTaskWoken == pdTRUE)
		portYIELD();
}

