/*
 *  @file VGFThermo/filters.c
 *  @date 24.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief Фильтр скользящего среднего окна.
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "filters.h"

/**
 * @function FilterInit
 * @param 	f
 * @retval 	None
 * @brief	Инициализирует буфер (обнуляет, то есть)
 */
void FilterInit(Filter *f) {
	f->position = 0;
	f->index_max = 0;
	f->index_min = 0;
	f->average = 0;
	memset(f->data, 0, WINDOW_SIZE);
}


/**
 * @function FilterCalc
 * @param	x - Входные данные
 * @retval
 */
int32_t FilterCalc(Filter *f, int32_t x) {
int32_t sum;

	f->data[f->position] = x;
	f->position++;
	if (f->position > WINDOW_SIZE - 1)
		f->position = 0;

	sum = 0;
	for (int i = 0; i < WINDOW_SIZE; i++) {
		sum += f->data[i];
	}

	f->average = sum / WINDOW_SIZE;
	return f->average;
}



