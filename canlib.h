/*
 *  @file VGFThermo/canlib.h
 *  @date 14.01.2013
 *  @author Vladimir Meshkov <glavmonter@gmail.com>
 *  @brief
 */
#ifndef CANLIB_H_
#define CANLIB_H_

#include <stm32f4xx.h>

typedef enum {
	CS_WAIT_START = 0,
	CS_SEND_TO_HOST = 1,
	CS_RECEIVE_FROM_HOST = 2
} CANStatus;

#define CAN_OK 				(0)
#define CAN_TIMEOUT_ERROR	(-1)
#define CAN_ACK_ERROR		(-2)
#define CAN_MEMORY_ERROR	(-3)
#define CAN_SEMPTH_ERROR	(-4)
#define CAN_PB_ERROR		(-5)

void CANLib_Init(uint8_t id);

#endif /* CANLIB_H_ */
