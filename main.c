/**
  ******************************************************************************
  * @file    VGFThermo/main.c
  * @author  Vladimir Meshkov <glavmonter@gmail.com>
  * @version V1.0.0
  * @date    4-December-2012
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <string.h>
#include "main.h"
#include "hardware.h"
#include "lmp90100.h"
#include "eeprom.h"
#include "canlib.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "controller.pb.h"
#include "onewire.h"

#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
xQueueHandle xUSARTQueue = NULL;
unsigned long ulRunTimeStatsClock;
__IO uint16_t uhADCConvertedValue = 0;

/* Private function prototypes -----------------------------------------------*/
portTASK_FUNCTION_PROTO(vLEDBlink, pvParameters);
portTASK_FUNCTION_PROTO(vUSART, pvParameters);

static void prvSetupHardware();
static size_t CheckFirsStart();
static void ADC_Config();

#if configGENERATE_RUN_TIME_STATS == 1
static void setupRunTimeStats();
#endif

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
int main(void) {
	prvSetupHardware();
#if configGENERATE_RUN_TIME_STATS == 1
	setupRunTimeStats();
#endif


uint8_t *buffer;
size_t count;
uint16_t DataRead;

	EEP_Init();
	count = CheckFirsStart();

	buffer = pvPortMalloc(EEPROM_SIZE);
pb_istream_t stream = pb_istream_from_buffer(buffer, count);
EepSettings settings;

	DataRead = count;
	EEP_ReadBuffer(buffer, EEPROM_OFFSET + sizeof(uint8_t) + sizeof(count), &DataRead);
	while (DataRead > 0);

	if (!pb_decode(&stream, EepSettings_fields, &settings)) {
		LED_RED(LED1);
		LED_RED(LED2);
		while (1) {}
	}

	ADC_Config();

	LMP_Init();
	CANLib_Init(4);
	OW_Init();

	xTaskCreate(vLEDBlink,  (signed portCHAR *)"LED1", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
	vPortFree(buffer);

	vTaskStartScheduler();

	/* Infinite loop */
	while (1)
	{}
	return 0;
}

/*
 *
 */
static size_t CheckFirsStart() {
uint16_t DataRead;
uint8_t *buffer;
size_t bytes_written;
uint8_t firstByte;

	DataRead = 1;
	EEP_ReadBuffer(&firstByte, EEPROM_OFFSET, &DataRead);
	while (DataRead > 0);

	DataRead = sizeof(bytes_written);
	EEP_ReadBuffer((uint8_t *)&bytes_written, EEPROM_OFFSET + sizeof(firstByte), &DataRead);
	while (DataRead > 0);

	/* Нулевой байт = 0xAA - Не первый запуск */
	if (firstByte == 0xAA) {
		/* Уже был первый запуск, выходим */
		return bytes_written;
	}

	/* Первый запуск, обнуляем все поля и пишем в EEPROM */
	buffer = (uint8_t *)pvPortMalloc(EEPROM_SIZE - EEPROM_OFFSET - sizeof(uint8_t) - sizeof(size_t));
	memset(buffer, 0, EEPROM_SIZE - EEPROM_OFFSET - sizeof(uint8_t) - sizeof(size_t));
pb_ostream_t stream = pb_ostream_from_buffer(buffer, EEPROM_SIZE - EEPROM_OFFSET - sizeof(uint8_t) - sizeof(size_t));
EepSettings settings;

	settings.HeatingTime_count = 8;
	for (int i = 0; i < settings.HeatingTime_count; i++) {
		settings.HeatingTime[i] = 0;
	}

	if (!pb_encode(&stream, EepSettings_fields, &settings))
		return 0;

	bytes_written = stream.bytes_written;

	firstByte = 0xAA;
	EEP_WriteBuffer(&firstByte, EEPROM_OFFSET, sizeof(uint8_t));
	EEP_WaitEepromStandbyState();

	EEP_WriteBuffer((uint8_t *)&bytes_written, EEPROM_OFFSET + sizeof(uint8_t), sizeof(bytes_written));
	EEP_WaitEepromStandbyState();

	EEP_WriteBuffer(buffer, EEPROM_OFFSET + sizeof(uint8_t) + sizeof(size_t), bytes_written);
	EEP_WaitEepromStandbyState();

	vPortFree(buffer);
	return bytes_written;
}

#if configGENERATE_RUN_TIME_STATS == 1

static void setupRunTimeStats() {
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = (uint16_t)(SystemCoreClock/20000) - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseStructure);

	TIM_ITConfig(TIM7, TIM_IT_Update, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM7_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_Cmd(TIM7, ENABLE);
}

void TIM7_IRQHandler() {
	if (TIM_GetITStatus(TIM7, TIM_IT_Update)) {
		TIM_ClearITPendingBit(TIM7, TIM_IT_Update);
		ulRunTimeStatsClock++;
	}
}
#endif

static void ADC_Config() {
	ADC_InitTypeDef	ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	DMA_InitTypeDef DMA_InitStructure;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	DMA_DeInit(DMA2_Stream0);
	DMA_InitStructure.DMA_Channel = DMA_Channel_0;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&uhADCConvertedValue;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = 1;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream0, &DMA_InitStructure);
	DMA_Cmd(DMA2_Stream0, ENABLE);

	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_20Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	ADC_DMACmd(ADC1, ENABLE);

	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_144Cycles);
	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
	ADC_Cmd(ADC1, ENABLE);
}


static void prvSetupHardware() {

	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |
							RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
							RCC_AHB1Periph_GPIOE, ENABLE);

GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = LED1_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(LED1_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED2_PIN;
	GPIO_Init(LED2_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED3_PIN;
	GPIO_Init(LED3_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = LED4_PIN;
	GPIO_Init(LED4_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(DBG_USART_CLK, ENABLE);
	GPIO_PinAFConfig(DBG_USART_RX_GPIO_PORT, DBG_USART_RX_SOURCE, DBG_USART_RX_AF);
	GPIO_PinAFConfig(DBG_USART_TX_GPIO_PORT, DBG_USART_TX_SOURCE, DBG_USART_TX_AF);

	/* USART1 Tx PA9 */
	GPIO_InitStructure.GPIO_Pin = DBG_USART_TX_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DBG_USART_TX_GPIO_PORT, &GPIO_InitStructure);

	/* USART1 Rx PA10 */
	GPIO_InitStructure.GPIO_Pin = DBG_USART_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(DBG_USART_RX_GPIO_PORT, &GPIO_InitStructure);

USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(DBG_USART, &USART_InitStructure);
	USART_Cmd(DBG_USART, ENABLE);
	xUSARTQueue = xQueueCreate(4, sizeof(uint8_t) * 50);
	xTaskCreate(vUSART,  (signed portCHAR *)"USART", configMINIMAL_STACK_SIZE, NULL, 4, NULL);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
}

portTASK_FUNCTION(vUSART, pvParameters) {
uint8_t buffer[50];
uint8_t len;

	for (;;) {
		if (xUSARTQueue != NULL) {
			if (xQueueReceive(xUSARTQueue, buffer, 0xffff) == pdTRUE) {
				len = strnlen(buffer, 50);
				for (int i = 0; i < len; i++) {
#if configGENERATE_RUN_TIME_STATS == 0
					USART_SendData(DBG_USART, (uint8_t) buffer[i]);
					while (USART_GetFlagStatus(DBG_USART, USART_FLAG_TC) == RESET);
#else
					__ASM volatile ("nop");
#endif
				}
			}
		}
	}
}


#if configGENERATE_RUN_TIME_STATS == 1
signed char buffer[48*10];
portTASK_FUNCTION(vLEDBlink, pvParameters) {
	for (;;) {
		vTaskDelay(2000);
		GPIO_ToggleBits(LED1_PORT, LED1_PIN);
		vTaskGetRunTimeStats(buffer);
		printf(buffer);
	}
}
#else
portTASK_FUNCTION(vLEDBlink, pvParameters) {

	ADC_SoftwareStartConv(ADC1);
	for (;;) {
//		float VSense = uhADCConvertedValue * 3300.0f/4096.0f;
//		float Temp = (VSense - 760.0f)/2.5f + 25.0f;
//		uint8_t buff[50];
//		memset(buff, 0, sizeof(buff));
//		int a = (int)Temp;
//		int b = (int)(Temp*10.0f) - a*10;
//		int c = (int)(Temp*100.0f) - a*100 - b*10;

//		sprintf(buff, "Temp: %d.%d%d\n", a, b, c);
//		xQueueSend(xUSARTQueue, buff, 5);

		vTaskDelay(100);
	}
}
#endif


void vApplicationStackOverflowHook(xTaskHandle xTask, signed portCHAR *pcTaskName) {
	LED_RED(LED1);
	for (;;) {}
}

void vApplicationMallocFailedHook( void ) {
	LED_RED(LED2);
	for (;;) {}
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
	/* User can add his own implementation to report the file name and line number,
		ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

#if configGENERATE_RUN_TIME_STATS == 1
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE {
	/* Place your implementation of fputc here */
	/* e.g. write a character to the USART */
	USART_SendData(DBG_USART, (uint8_t) ch);

	/* Loop until the end of transmission */
	while (USART_GetFlagStatus(DBG_USART, USART_FLAG_TC) == RESET)
	{}

	return ch;
}
#else
PUTCHAR_PROTOTYPE {
	/* Place your implementation of fputc here */
	/* e.g. write a character to the USART */
	/* Loop until the end of transmission */
	return ch;
}

#endif

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
