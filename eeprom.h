/**
  ******************************************************************************
  * @file    eeprom.h
  * @author  MCD Application Team
  * @version V1.0.2
  * @date    09-March-2012
  * @brief   This file contains all the functions prototypes for the 
  *          stm324xg_eval_i2c_ee.c driver.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEPROM_H
#define __EEPROM_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/* Uncomment the following line to use the default sEE_TIMEOUT_UserCallback() 
   function implemented in stm32_evel_i2c_ee.c file.
   sEE_TIMEOUT_UserCallback() function is called whenever a timeout condition 
   occure during communication (waiting on an event that doesn't occur, bus 
   errors, busy devices ...). */   
#define USE_DEFAULT_TIMEOUT_CALLBACK


#if !defined (EEP_M24C08) && !defined (EEP_M24C64_32)
/* Use the defines below the choose the EEPROM type */
#define EEP_M24C08  /* Support the device: M24C08. */
/* note: Could support: M24C01, M24C02, M24C04 and M24C16 if the blocks and 
   HW address are correctly defined*/
//#define EEP_M24C64_32  /* Support the devices: M24C32 and M24C64 */
#endif

#ifdef EEP_M24C64_32
/* For M24C32 and M24C64 devices, E0,E1 and E2 pins are all used for device 
  address selection (ne need for additional address lines). According to the 
  Harware connection on the board (on STM324xG-EVAL board E0 = E1 = E2 = 0) */

 #define EEP_HW_ADDRESS         0xA0   /* E0 = E1 = E2 = 0 */

#elif defined (EEP_M24C08)
/* The M24C08W contains 4 blocks (128byte each) with the adresses below: E2 = 0 
   EEPROM Addresses defines */
 #define EEP_Block0_ADDRESS     0xA0   /* E2 = 0 */
 /*#define EEP_Block1_ADDRESS     0xA2*/ /* E2 = 0 */
 /*#define EEP_Block2_ADDRESS     0xA4*/ /* E2 = 0 */
 /*#define EEP_Block3_ADDRESS     0xA6*/ /* E2 = 0 */

#endif /* EEP_M24C64_32 */

/* I2C clock speed configuration (in Hz) 
  WARNING: 
   Make sure that this define is not already declared in other files (ie. 
  stm324xg_eval.h file). It can be used in parallel by other modules. */
#ifndef I2C_SPEED
 #define I2C_SPEED                        100000
#endif /* I2C_SPEED */

#define I2C_SLAVE_ADDRESS7      0xA0

#if defined (EEP_M24C08)
 #define EEP_PAGESIZE           16
#elif defined (EEP_M24C64_32)
 #define EEP_PAGESIZE           32
#endif
   
/* Maximum Timeout values for flags and events waiting loops. These timeouts are
   not based on accurate values, they just guarantee that the application will 
   not remain stuck if the I2C communication is corrupted.
   You may modify these timeout values depending on CPU frequency and application
   conditions (interrupts routines ...). */   
#define EEP_FLAG_TIMEOUT         ((uint32_t)0x1000)
#define EEP_LONG_TIMEOUT         ((uint32_t)(10 * EEP_FLAG_TIMEOUT))

/* Maximum number of trials for sEE_WaitEepromStandbyState() function */
#define EEP_MAX_TRIALS_NUMBER     300
   
/* Defintions for the state of the DMA transfer */   
#define EEP_STATE_READY           0
#define EEP_STATE_BUSY            1
#define EEP_STATE_ERROR           2
   
#define EEP_OK                    0
#define EEP_FAIL                  1

#define EEPROM_SIZE				512
#define EEPROM_OFFSET			8

void     EEP_DeInit(void);
void     EEP_Init(void);
uint32_t EEP_ReadBuffer(uint8_t *pBuffer, uint16_t ReadAddr, uint16_t *NumByteToRead);
uint32_t EEP_WritePage(uint8_t *pBuffer, uint16_t WriteAddr, uint8_t *NumByteToWrite);
void     EEP_WriteBuffer(uint8_t *pBuffer, uint16_t WriteAddr, uint16_t NumByteToWrite);
uint32_t EEP_WaitEepromStandbyState(void);

/* USER Callbacks: These are functions for which prototypes only are declared in
   EEPROM driver and that should be implemented into user applicaiton. */  
/* sEE_TIMEOUT_UserCallback() function is called whenever a timeout condition 
   occure during communication (waiting on an event that doesn't occur, bus 
   errors, busy devices ...).
   You can use the default timeout callback implementation by uncommenting the 
   define USE_DEFAULT_TIMEOUT_CALLBACK in stm324xg_eval_i2c_ee.h file.
   Typically the user implementation of this callback should reset I2C peripheral
   and re-initialize communication or in worst case reset all the application. */
uint32_t EEP_TIMEOUT_UserCallback(void);

#ifdef __cplusplus
}
#endif

#endif /* __EEPROM_H */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/


