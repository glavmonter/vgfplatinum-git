/*
 * onewire.c
 *
 *  Created on: 12 нояб. 2013 г.
 *      Author: sony
 */


#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <string.h>
#include <stdio.h>
#include "hardware.h"
#include "onewire.h"

portTASK_FUNCTION_PROTO(vOWTask, pvParameters);
void OW_WriteByte(uint8_t data);

DS12B20Device Devices[] = {{.ROM = {0x28, 0x7A, 0xA3, 0x3E, 0x05, 0x00, 0x00, 0x5C}, .Temperature = 0.0f},
							{.ROM = {0x28, 0xAD, 0xA9, 0x3E, 0x05, 0x00, 0x00, 0xA0}, .Temperature = 0.0f},
							{.ROM = {0x28, 0x05, 0xA3, 0x36, 0x05, 0x00, 0x00, 0xFC}, .Temperature = 0.0f},
							{.ROM = {0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, .Temperature = 0.0f},
							{.ROM = {0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, .Temperature = 0.0f},
							{.ROM = {0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, .Temperature = 0.0f},
							{.ROM = {0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, .Temperature = 0.0f},
							{.ROM = {0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, .Temperature = 0.0f}};

__IO uint8_t TxBuffer[8];
uint8_t RxBuffer[8];
xQueueHandle xUSARTQueue;

void OW_Init() {
	/* Enable USART2 CLK */
	WIRE_USART_CLK_INIT(WIRE_USART_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	GPIO_PinAFConfig(WIRE_USART_TX_GPIO_PORT, WIRE_USART_TX_SOURCE, WIRE_USART_TX_AF);
	GPIO_PinAFConfig(WIRE_USART_RX_GPIO_PORT, WIRE_USART_RX_SOURCE, WIRE_USART_RX_AF);

GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = WIRE_USART_TX_PIN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(WIRE_USART_TX_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = WIRE_USART_RX_PIN;
	GPIO_Init(WIRE_USART_RX_GPIO_PORT, &GPIO_InitStructure);

USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = BAUD_RATE_NORMAL;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(WIRE_USART, &USART_InitStructure);
	USART_Cmd(WIRE_USART, ENABLE);

	xTaskCreate(vOWTask, (signed portCHAR *)"OW", configMINIMAL_STACK_SIZE*3, NULL, 3, NULL);
}

/**
 * @function OW_Reset
 * @brief Сброс шины 1-Wire
 * @param None
 * @retval OW_DEVICES_YES - если есть устройства на шине, OW_DEVICES_NO - в противном случае
 */
uint8_t OW_Reset() {
USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = BAUD_RATE_RESET;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(WIRE_USART, &USART_InitStructure);
	USART_ClearFlag(WIRE_USART, USART_FLAG_TC);

	USART_SendData(WIRE_USART, 0xF0);
	while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET);
	uint8_t ret = USART_ReceiveData(WIRE_USART);

	USART_InitStructure.USART_BaudRate = BAUD_RATE_NORMAL;
	USART_Init(WIRE_USART, &USART_InitStructure);
	USART_ClearFlag(WIRE_USART, USART_FLAG_TC);

	if (ret != 0xF0)
		return OW_DEVICES_YES; /* Есть устройство! */
	return OW_DEVICES_NO; /* Нет устройства */
}


void OW_WriteByteDMA(uint8_t data) {
	for (int i = 0; i < 8; i++) {
		if (data & 0x01)
			TxBuffer[i] = 0xFF;
		else
			TxBuffer[i] = 0x00;
		data >>= 1;
	}

DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_BufferSize = 8;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&WIRE_USART->DR;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;

	DMA_InitStructure.DMA_Channel = DMA_Channel_4;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)TxBuffer;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	DMA_Cmd(DMA1_Stream6, ENABLE);
	USART_DMACmd(WIRE_USART, USART_DMAReq_Tx, ENABLE);

	while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET);
	while (DMA_GetFlagStatus(DMA1_Stream6, DMA_FLAG_TCIF6) == RESET);

	DMA_ClearFlag(DMA1_Stream6, DMA_FLAG_TCIF6);
	USART_ClearFlag(WIRE_USART, USART_FLAG_TC);
	USART_DMACmd(WIRE_USART, USART_DMAReq_Tx, DISABLE);
}

uint8_t OW_ReadByteDMA() {
	memset(RxBuffer, 0, 8);

DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_BufferSize = 8;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&WIRE_USART->DR;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;

	DMA_InitStructure.DMA_Channel = DMA_Channel_4;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)RxBuffer;
	DMA_Init(DMA1_Stream5, &DMA_InitStructure);

	DMA_Cmd(DMA1_Stream5, ENABLE);
	USART_DMACmd(WIRE_USART, USART_DMAReq_Rx, ENABLE);

	OW_WriteByte(0xFF);

	while (DMA_GetFlagStatus(DMA1_Stream5, DMA_FLAG_TCIF5) == RESET);
	DMA_ClearFlag(DMA1_Stream5, DMA_FLAG_TCIF5);
	USART_ClearFlag(WIRE_USART, USART_FLAG_TC);
	USART_DMACmd(WIRE_USART, USART_DMAReq_Rx, DISABLE);

	uint8_t data = 0x00;
	for (int i = 0; i < 8; i++) {
		data >>= 1;
		if (RxBuffer[i] == 255)
			data |= 0x80;
	}
	return data;
}

/**
 * @function OW_WriteByte
 * @brief Запись байта в шину
 * @
 */
void OW_WriteByte(uint8_t data) {
	for (int i = 0; i < 8; i++) {
		if (data & 0x01)
			USART_SendData(WIRE_USART, 0xFF); // Запись 1
		else
			USART_SendData(WIRE_USART, 0x00); // Запись 0
		while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET);
		data >>= 1;
	}
}

/**
 *
 */
uint8_t OW_ReadByte() {
	uint8_t data = 0x00;
	for (int i = 0; i < 8; i++) {
		USART_SendData(WIRE_USART, 0xFF);
		while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_TC) == RESET);
		while (USART_GetFlagStatus(WIRE_USART, USART_FLAG_RXNE) == RESET);

		uint8_t ret = USART_ReceiveData(WIRE_USART);
		data >>= 1;
		if (ret == 255)
			data |= 0x80;
	}
	return data;
}

uint8_t OW_ComputeCRC8(unsigned char inData, unsigned char seed)
{
	uint8_t bitsLeft;
	uint8_t temp;

	for (bitsLeft = 8; bitsLeft > 0; bitsLeft--) {
		temp = ((seed ^ inData) & 0x01);
		if (temp == 0) {
			seed >>= 1;
		} else {
			seed ^= 0x18;
			seed >>= 1;
			seed |= 0x80;
		}
		inData >>= 1;
	}
	return seed;
}

uint8_t OW_CalcRomCRC(uint8_t *data) {
uint8_t crc8 = 0;

	for (int i = 0; i < 7; i++) {
		crc8 = OW_ComputeCRC8(*data, crc8);
		data++;
	}
	return crc8;
}

uint8_t OW_CalcScratchCRC(uint8_t *data) {
uint8_t crc8 = 0;
	for (int i = 0; i < 8; i++) {
		crc8 = OW_ComputeCRC8(*data, crc8);
		data++;
	}
	return crc8;
}



float ReadTemperature(DS12B20Device *device) {
float fTemp;
uint16_t ulTemp;
uint8_t scratch[9];

	OW_Reset();
	OW_WriteByte(OW_ROM_MATCH);
	for (int i = 0; i < 8; i++)
		OW_WriteByte(device->ROM[i]);

	OW_WriteByte(0xBE); // Read scratch
	for (int i = 0; i < 9; i++)
		scratch[i] = OW_ReadByte();

	if (OW_CalcScratchCRC(scratch) != scratch[8])
		return device->Temperature;

	ulTemp = scratch[0];
	ulTemp |= scratch[1] << 8;
	if (ulTemp > 2097) {
		ulTemp = 65535 - ulTemp;
		fTemp = -(((ulTemp & 0x7F0) >> 4)*1.0f + ((ulTemp & 0xF)*0.0625f));
	} else {
		fTemp = ((ulTemp & 0x7F0) >> 4)*1.0f + (ulTemp & 0xF)*0.0625f;
	}

	device->Temperature = fTemp;
	return fTemp;
}

portTASK_FUNCTION(vOWTask, pvParameters) {
(void) pvParameters;

	if (OW_Reset() == OW_DEVICES_NO)
		vTaskDelete(NULL);

	for (;;) {
		OW_Reset();
		OW_WriteByte(OW_ROM_SKIP);
		OW_WriteByte(0x44); // Start convertion
		vTaskDelay(1000);

		for (int i = 0; i < sizeof(Devices)/sizeof(DS12B20Device); i++) {
			int a, b, c, d, e;
			int8_t buf[50];
			float fTemp = ReadTemperature(&Devices[i]);

			a = (int)fTemp;
			b = fTemp*10 - a*10;
			c = fTemp*100 - a*100 - b*10;
			d = fTemp*1000 - a*1000 - b*100 - c*10;
			e = fTemp*10000 - a*10000 - b*1000 - c*100 - d*10;
			sprintf(buf, "Dev[%d] %d.%d%d%d%d\n", i, a, b, c, d, e);
			xQueueSend(xUSARTQueue, buf, 10);
		}
		xQueueSend(xUSARTQueue, "\n", 10);
	}
}
