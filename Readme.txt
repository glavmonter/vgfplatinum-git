Как собрать проект.
cd project_path
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=/opt/cmake/gcc_stm32f4xx.cmake -DCMAKE_BUILD_TYPE=Debug -G"Eclipse CDT4 - Unix Makefiles" ..
make

При объявлении configGENERATE_RUN_TIME_STATS 1 в FreeRTOSConfig.h генерируется статистика процессов и выбрасывается на USART.
Для вывода служебных сообщений в USART использовать xQueueSend(xUSARTQueue, bla_buffer, wait_time);

Запуск отладки на Linux. Zylin Embedded debug (Native) + init commands:

target remote localhost:2331
monitor interface SWD
monitor speed 10000
monitor sleep 100
monitor halt
set remote memory-write-packet-size 1024
set remote memory-write-packet-size fixed
monitor flash device = STM32F407VE
monitor flash download = 1
set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 4
monitor sleep 100
monitor endian little
monitor sleep 10
load 
monitor reset
#break SystemInit
break main
continue

